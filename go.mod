module go-gentoo

go 1.15

require (
	github.com/catinello/base62 v0.0.0-20160325105823-e0daaeb631c9
	github.com/coreos/go-oidc v2.2.1+incompatible
	github.com/go-pg/pg/v10 v10.7.3 // indirect
	github.com/go-pg/pg/v9 v9.2.0
	github.com/google/go-github v17.0.0+incompatible
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/gorilla/sessions v1.2.1
	github.com/pquerna/cachecontrol v0.0.0-20201205024021-ac21108117ac // indirect
	golang.org/x/oauth2 v0.0.0-20201207163604-931764155e3f
	gopkg.in/square/go-jose.v2 v2.5.1 // indirect
)
