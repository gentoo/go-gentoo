package links

import (
	"go-gentoo/pkg/app/handler/auth"
	"html/template"
	"net/http"
)

// Show renders a template to show the landing page of the application
func Create(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		showForm(w, r)
	case http.MethodPost:
		createLink(w, r)
	}
}

func showForm(w http.ResponseWriter, r *http.Request) {

	templates := template.Must(
		template.Must(
			template.New("Show").
				Funcs(getFuncMap()).
				ParseGlob("web/templates/layout/*.tmpl")).
			ParseGlob("web/templates/links/create.tmpl"))

	templates.ExecuteTemplate(w, "create.tmpl", getPageData(w, r, "create"))
}

func createLink(w http.ResponseWriter, r *http.Request) {
	user := auth.GetUser(w, r)

	r.ParseForm()
	prefix := r.Form.Get("prefix")
	index := r.Form.Get("index")
	token := r.Form.Get("token")
	target := r.Form.Get("target")

	if !user.IsAdmin() && prefix == "" {
		token = ""
	}

	if isValidPrefix(user, prefix) && isValidToken(prefix) && isValidToken(token) && isValidTarget(target) {
		shortlink, ok := createShortURL(prefix, token, target, user.Email, index == "yes", 0)
		if ok {
			renderCreatedTemplate(w, r, shortlink, target)
			return
		} else {
			http.Error(w, "Could not create shortened URL", http.StatusInternalServerError)
		}
	} else {
		http.Error(w, "Params are not valid", http.StatusBadRequest)
	}
}

func renderCreatedTemplate(w http.ResponseWriter, r *http.Request, shortlink, target string) {
	templates := template.Must(
		template.Must(
			template.New("Show").
				Funcs(getFuncMap()).
				ParseGlob("web/templates/layout/*.tmpl")).
			ParseGlob("web/templates/links/created.tmpl"))

	templates.ExecuteTemplate(w, "created.tmpl", getCreatePageData(w, r, shortlink, target))
}
