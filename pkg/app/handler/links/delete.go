package links

import (
	"go-gentoo/pkg/app/handler/auth"
	"go-gentoo/pkg/database"
	"go-gentoo/pkg/models"
	"net/http"
)

func Delete(w http.ResponseWriter, r *http.Request) {
	user := auth.GetUser(w, r)

	r.ParseForm()
	prefix := r.Form.Get("prefix")
	token := r.Form.Get("token")
	from := r.Form.Get("from")
	var shortlink string
	if prefix != "" {
		shortlink = "/" + prefix + "/" + token
	} else {
		shortlink = "/" + token
	}

	links := getLink(shortlink)

	if len(links) != 1 {
		http.Error(w, "Could not delete shortened URL", http.StatusInternalServerError)
		return
	}

	if user.IsAdmin() || links[0].UserEmail == user.Email || contains(user.Projects, links[0].Prefix) || links[0].Prefix == user.UserName {
		link := new(models.Link)
		_, err := database.DBCon.Model(link).Where("short_link = ?", shortlink).Delete()
		if err != nil {
			http.Error(w, "Could not delete shortened URL", http.StatusInternalServerError)
		} else {
			http.Redirect(w, r, from, http.StatusFound)
		}
	}

}
