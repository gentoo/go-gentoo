package config

import "os"

func PostgresUser() string {
	return getEnv("GO_GENTOO_POSTGRES_USER", "root")
}

func PostgresPass() string {
	return getEnv("GO_GENTOO_POSTGRES_PASS", "root")
}

func PostgresDb() string {
	return getEnv("GO_GENTOO_POSTGRES_DB", "gogentoo")
}

func PostgresHost() string {
	return getEnv("GO_GENTOO_POSTGRES_HOST", "db")
}

func PostgresPort() string {
	return getEnv("GO_GENTOO_POSTGRES_PORT", "5432")
}

func Port() string {
	return getEnv("GO_GENTOO_PORT", "5000")
}

func CacheControl() string {
	return getEnv("GO_GENTOO_CACHE_CONTROL", "max-age=300")
}

func Debug() string {
	return getEnv("GO_GENTOO_DEBUG", "false")
}

func LogFile() string {
	return getEnv("GO_GENTOO_LOG_FILE", "/var/log/go-gentoo/errors.log")
}

func OIDConfigURL() string {
	return getEnv("GO_GENTOO_OID_CONFIG_URL", "https://sso.gentoo.org/auth/realms/gentoo")
}

func OIDClientID() string {
	return getEnv("GO_GENTOO_OID_CLIENT_ID", "demo-client")
}

func OIDClientSecret() string {
	return getEnv("GO_GENTOO_OID_CLIENT_SECRET", "00000000-0000-0000-0000-000000000000")
}

func ApplicationURL() string {
	return getEnv("GO_GENTOO_APPLICATION_URL", "https://go.gentoo.org")
}

func SessionStoreKey() string {
	return getEnv("GO_GENTOO_SESSION_STORE_KEY", "gentoo_sess")
}

func SessionSecret() string {
	return getEnv("GO_GENTOO_SESSION_SECRET", "123456789")
}

func ValidURLTokenChars() string {
	return getEnv("GO_GENTOO_VALID_URL_TOKEN_CHARS", "abcdefghijklmnopqrstuvwxyz1234567890-")
}

func getEnv(key string, fallback string) string {
	if os.Getenv(key) != "" {
		return os.Getenv(key)
	} else {
		return fallback
	}
}
