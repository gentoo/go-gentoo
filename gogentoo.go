package main

import (
	"flag"
	"go-gentoo/pkg/app"
	"go-gentoo/pkg/config"
	"go-gentoo/pkg/logger"
	"io"
	"io/ioutil"
	"os"
	"time"
)

func main() {

	waitForPostgres()

	errorLogFile := logger.CreateLogFile(config.LogFile())
	defer errorLogFile.Close()
	initLoggers(os.Stdout, errorLogFile)

	serve := flag.Bool("serve", false, "Start serving the application")
	help := flag.Bool("help", false, "Print the usage of this application")
	flag.Parse()

	if *serve {
		app.Serve()
	}

	if *help {
		flag.PrintDefaults()
	}
}

// initialize the loggers depending on whether
// config.debug is set to true
func initLoggers(infoHandler io.Writer, errorHandler io.Writer) {
	if config.Debug() == "true" {
		logger.Init(os.Stdout, infoHandler, errorHandler)
	} else {
		logger.Init(ioutil.Discard, infoHandler, errorHandler)
	}
}

// TODO this has to be solved differently
// wait for postgres to come up
func waitForPostgres() {
	time.Sleep(5 * time.Second)
}

// Login

// Logout

// Create New
//  - [optional] select project
//  - [optional] select expire
//  - [optional] select path

// View existing
//  - personal
//  - per Project
// => options per entry
//  - change expire
//  - delete
//  (- add new)
